package org.fdroid.fdroid.data;

import android.content.Context;
import android.net.Uri;

/**
 * Placeholder to avoid modifying classes copied from fdroidclient.
 */
public class AppProvider {

    public static Uri PLACEHOLDER_URI =
            Uri.parse("content://" + FDroidProvider.AUTHORITY + ".InstalledAppProvider/placeholder");

    public static class Helper {
        public static void calcSuggestedApk(Context context, String packageName) {
            // no op
        }
    }

    public static Uri getContentUri() {
        return PLACEHOLDER_URI;
    }

    public static Uri getHighestPriorityMetadataUri(String packageName) {
        return PLACEHOLDER_URI;
    }

    public static Uri getCanUpdateUri() {
        return PLACEHOLDER_URI;
    }
}
